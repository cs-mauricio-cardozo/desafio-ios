//
//  GitHub.swift
//  ConcreteSolutionsChallenge
//
//  Created by Mauricio Cardozo on 10/30/17.
//  Copyright © 2017 Mauricio Cardozo. All rights reserved.
//

import Foundation

final class GitHub {

    static let apiURL: URL = URL(string: "https://api.github.com")!

    enum SortingBy: String {
        case forks
        case stars
    }

    enum Ordered: String {
        case ascending = "asc"
        case descending = "desc"
    }

    enum PRStatus: String {
        case open
        case closed
    }

    /// composes URL for GitHub's repository fetching service
    static func composeURL(language: ProgrammingLanguages,
                           sort: SortingBy = .forks,
                           order: Ordered = .descending,
                           pageNumber: Int = 1) -> URL {
        // swiftlint:disable:next line_length
        return URL(string: "search/repositories?q=language:\(language.rawValue.lowercased())&sort=\(sort)&order=\(order.rawValue)&page=\(String(pageNumber))",
            relativeTo: apiURL)!
    }

    /// composes URL for GitHub's pull request fetching service
    fileprivate static func composeURL(fullPath: String, pageNumber: Int = 1) -> URL {
        return URL(string: "repos/\(fullPath)/pulls?state=all&page=\(String(pageNumber))", relativeTo: apiURL)!
    }

    /// composes URL for GitHub's pull request fetching service
    static func composeURL(owner: String, repo: String, pageNumber: Int = 1) -> URL {
        return composeURL(fullPath: "\(owner)/\(repo)", pageNumber: pageNumber)
    }

    /// composes URL for GitHub's pull request fetching service
    static func composeURL(for repository: Repository, pageNumber: Int = 1) -> URL {
        return composeURL(fullPath: repository.fullName)
    }

    /// composes URL for GItHub's pull request status fetching service
    static func composeURL(for repository: Repository, status: PRStatus) -> URL {
        return URL(string: "search/issues?q=type:pr+repo:\(repository.fullName.lowercased())+is:\(status)",
            relativeTo: apiURL)!
    }

    static func composeUserURL(for repository: Repository) -> URL {
        return composeUserURL(username: repository.author.username.lowercased())
    }

    static func composeUserURL(username: String) -> URL {
        return URL(string: "users/\(username)", relativeTo: apiURL)!
    }

}
