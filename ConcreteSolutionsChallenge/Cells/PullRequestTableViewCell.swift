//
//  PullRequestTableViewCell.swift
//  ConcreteSolutionsChallenge
//
//  Created by Mauricio Cardozo on 11/5/17.
//  Copyright © 2017 Mauricio Cardozo. All rights reserved.
//

import Foundation
import UIKit

class PullRequestTableViewCell: UITableViewCell {

    static let rowHeight = CGFloat(120)

    @IBOutlet fileprivate weak var title: UILabel!
    @IBOutlet fileprivate weak var body: UILabel!
    @IBOutlet fileprivate weak var authorPhoto: UIImageView!
    @IBOutlet fileprivate weak var authorUsername: UILabel!
    @IBOutlet fileprivate weak var authorName: UILabel!
    @IBOutlet fileprivate weak var requestDate: UILabel!

    var provider: UserProvider?

    func setRequest(for request: PullRequest) {
        self.title.text = request.title
        self.body.text = request.description
        self.authorUsername.text = request.author.username
        self.authorName.text = request.author.fullName
        self.requestDate.text = request.dateCreated
        request.author.avatarImage { (image) in
            guard let image = image else {
                return //uses xib default image
            }
            self.authorPhoto.image = image
        }
        if provider == nil {
            provider = UserService()
        }

        provider?.fetchUser(username: request.author.username, result: { (result) in
            switch result {
            case .success(let newUser):
                self.authorName.text = newUser.fullName
            default:
                break
            }
        })

    }
}
