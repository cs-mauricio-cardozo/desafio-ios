//
//  GitHubUser.swift
//  ConcreteSolutionsChallenge
//
//  Created by Mauricio Cardozo on 10/30/17.
//  Copyright © 2017 Mauricio Cardozo. All rights reserved.
//

import Foundation
import UIKit

struct GitHubUser: Codable, DataDecodable {
    let username: String
    let fullName: String
    let avatarUrlString: String

    init(from decoder: Decoder) throws {
        let raw = try RawGitHubUser(from: decoder)
        self.username = raw.login
        self.fullName = raw.name ?? ""
        self.avatarUrlString = raw.avatar_url
    }

    func avatarImage(callback: @escaping (UIImage?) -> Void) {
        DispatchQueue.global(qos: .background).async {
            do {
                let data = try Data(contentsOf: URL(string: self.avatarUrlString)!)
                let image = UIImage(data: data)
                DispatchQueue.main.sync {
                    callback(image)
                }
            } catch {
                callback(nil)
            }
        }
    }

}

extension GitHubUser {
    // swiftlint:disable identifier_name
    fileprivate struct RawGitHubUser: Codable {
        let login: String
        let avatar_url: String
        let name: String?
    }
}

/*

 {
 "login": "j-baker",
 "id": 42533,
 "avatar_url": "https://avatars2.githubusercontent.com/u/42533?v=4",
 "gravatar_id": "",
 "url": "https://api.github.com/users/j-baker",
 "html_url": "https://github.com/j-baker",
 "followers_url": "https://api.github.com/users/j-baker/followers",
 "following_url": "https://api.github.com/users/j-baker/following{/other_user}",
 "gists_url": "https://api.github.com/users/j-baker/gists{/gist_id}",
 "starred_url": "https://api.github.com/users/j-baker/starred{/owner}{/repo}",
 "subscriptions_url": "https://api.github.com/users/j-baker/subscriptions",
 "organizations_url": "https://api.github.com/users/j-baker/orgs",
 "repos_url": "https://api.github.com/users/j-baker/repos",
 "events_url": "https://api.github.com/users/j-baker/events{/privacy}",
 "received_events_url": "https://api.github.com/users/j-baker/received_events",
 "type": "User",
 "site_admin": false,
 "name": "James Baker",
 "company": null,
 "blog": "",
 "location": "London, UK",
 "email": null,
 "hireable": null,
 "bio": null,
 "public_repos": 40,
 "public_gists": 1,
 "followers": 23,
 "following": 9,
 "created_at": "2008-12-24T18:21:35Z",
 "updated_at": "2017-10-31T14:17:54Z"
 }

 */
