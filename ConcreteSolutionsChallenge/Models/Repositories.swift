//
//  Repositories.swift
//  ConcreteSolutionsChallenge
//
//  Created by Mauricio Cardozo on 10/31/17.
//  Copyright © 2017 Mauricio Cardozo. All rights reserved.
//

import Foundation

struct Repositories: Decodable, DataDecodable {
    var count: Int
    var incomplete: Bool
    var items: [Repository]

    init(from decoder: Decoder) throws {
        let rawResponse = try RawRepositories(from: decoder)
        count = rawResponse.total_count
        incomplete = rawResponse.incomplete_results
        items = rawResponse.items
    }

}

// helper struct for Repositories JSON decoding
// swiftlint:disable identifier_name
extension Repositories {
    fileprivate struct RawRepositories: Decodable, APIResponse {
        var total_count: Int
        var incomplete_results: Bool
        var items: [Repository]
    }
}
