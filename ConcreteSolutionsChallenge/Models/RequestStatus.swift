//
//  RequestStatus.swift
//  ConcreteSolutionsChallenge
//
//  Created by Mauricio Cardozo on 11/5/17.
//  Copyright © 2017 Mauricio Cardozo. All rights reserved.
//

import Foundation

struct RequestStatus: DataDecodable {
    var count: Int
    init(from decoder: Decoder) throws {
        let raw = try RawRequestStatus(from: decoder)
        self.count = raw.total_count
    }
}

extension RequestStatus: Decodable {
    fileprivate struct RawRequestStatus: Decodable {
        // swiftlint:disable identifier_name
        var total_count: Int
    }
}
