//
//  PullRequestListDataSource.swift
//  ConcreteSolutionsChallenge
//
//  Created by Mauricio Cardozo on 11/3/17.
//  Copyright © 2017 Mauricio Cardozo. All rights reserved.
//

import UIKit

class PullRequestListDataSource: NSObject {
    var data: [PullRequest]
    fileprivate let cellIdentifier = "PullRequestTableViewCell"

    init(_ tableView: UITableView, requests: [PullRequest]) {
        tableView.register(UINib(nibName: cellIdentifier, bundle: Bundle.main), forCellReuseIdentifier: cellIdentifier)
        data = requests
    }
}

extension PullRequestListDataSource: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let request = data[indexPath.row]
        if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
                                                                    as? PullRequestTableViewCell {
            cell.setRequest(for: request)
            return cell
        } else {
            let newCell = PullRequestTableViewCell(style: .default,
                                   reuseIdentifier: cellIdentifier)
            return newCell
        }
    }
}
