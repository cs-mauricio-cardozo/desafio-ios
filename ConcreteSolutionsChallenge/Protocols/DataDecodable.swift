//
//  DataDecodable.swift
//  ConcreteSolutionsChallenge
//
//  Created by Mauricio Cardozo on 11/3/17.
//  Copyright © 2017 Mauricio Cardozo. All rights reserved.
//

import Foundation

protocol DataDecodable {
    init(from data: Data) throws
}

extension DataDecodable where Self:Decodable {
    init(from data: Data) throws {
        let decoder = JSONDecoder()
        self = try decoder.decode(Self.self, from: data)
    }
}
