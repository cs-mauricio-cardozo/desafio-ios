//
//  APIResponse.swift
//  ConcreteSolutionsChallenge
//
//  Created by Mauricio Cardozo on 11/5/17.
//  Copyright © 2017 Mauricio Cardozo. All rights reserved.
//

import Foundation

// swiftlint:disable type_name
// swiftlint:disable identifier_name
protocol APIResponse {
    associatedtype T
    var total_count: Int { get set }
    var incomplete_results: Bool { get set }
    var items: [T] { get set }
}
