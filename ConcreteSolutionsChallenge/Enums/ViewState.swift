//
//  ViewState.swift
//  ConcreteSolutionsChallenge
//
//  Created by Mauricio Cardozo on 11/4/17.
//  Copyright © 2017 Mauricio Cardozo. All rights reserved.
//

import Foundation

enum ViewState {
    case loading
    case ready
    case error(String)
    case paginating
    case empty
}

extension ViewState: Equatable {
    func equals(otherState: ViewState) -> Bool {
        switch (self, otherState) {
        case (.loading, .loading),
             (.ready, .ready),
             (.error, .error),
             (.paginating, .paginating),
             (.empty, .empty):
            return true
        case (.loading, _),
             (.ready, _),
             (.error, _),
             (.paginating, _),
             (.empty, _):
            return false
        }
    }

    static func == (lhs: ViewState, rhs: ViewState) -> Bool {
        return lhs.equals(otherState: rhs)
    }
}
