//
//  PullRequestService.swift
//  ConcreteSolutionsChallenge
//
//  Created by Mauricio Cardozo on 10/30/17.
//  Copyright © 2017 Mauricio Cardozo. All rights reserved.
//

import Foundation

class PullRequestService {
    fileprivate var mSession: URLSession?
}

extension PullRequestService: PullRequestProvider {
    var session: URLSession {
        get {
            return mSession ?? URLSession.shared
        }
        set {
            self.mSession = newValue
        }
    }

    func fetchRequestStatus(for repository: Repository, status: GitHub.PRStatus,
                            result: @escaping (Result<(RequestStatus)>) -> Void) {
        session.dataTask(with: GitHub.composeURL(for: repository, status: status)) { (data, _, error) in
            guard let data = data else {
                DispatchQueue.main.async {
                    result(.error(PullRequestError()))
                }
                return
            }

            let decoder = JSONDecoder()
            do {
                let request = try decoder.decode(RequestStatus.self, from: data)
                DispatchQueue.main.async {
                    result(.success(request))
                }
            } catch {
                DispatchQueue.main.async {
                    result(.error(PullRequestError()))
                }
            }
        }.resume()
    }

    func fetchRequests(for repository: Repository, pageNumber: Int,
                       result: @escaping (Result<[PullRequest]>) -> Void) {
        URLSession.shared.dataTask(with: GitHub.composeURL(for: repository, pageNumber: pageNumber)) { data, _, error in
            guard let data = data else {
                DispatchQueue.main.async {
                    result(.error(PullRequestError()))
                }
                return
            }

            let decoder = JSONDecoder()
            do {
                let repos = try decoder.decode([PullRequest].self, from: data)
                DispatchQueue.main.async {
                    result(.success(repos))
                }
            } catch {
                DispatchQueue.main.async {
                    result(.error(PullRequestError()))
                }
            }
        }.resume()
    }
}

struct PullRequestError: Error {}
