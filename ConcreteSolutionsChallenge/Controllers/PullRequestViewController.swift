//
//  PullRequestsViewController.swift
//  ConcreteSolutionsChallenge
//
//  Created by Mauricio Cardozo on 11/2/17.
//  Copyright © 2017 Mauricio Cardozo. All rights reserved.
//

import Foundation
import UIKit

class PullRequestViewController: UIViewController {

    // nao esquecer de botar o header com count
    // de open/closed requests

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var requestCountLabel: UILabel!

    var repository: Repository?
    var requests: [PullRequest] = []
    var provider: PullRequestProvider? = PullRequestService()
    var dataSource: PullRequestListDataSource?
    // swiftlint:disable:next weak_delegate
    var delegate: PullRequestListTableViewDelegate?

    var currentState: ViewState = .loading {
        didSet {
            switch currentState {
            case .loading:
                self.tableView.separatorStyle = .none
                self.requestCountLabel.text = "Carregando"
            case .ready:
                self.tableView.separatorStyle = .singleLine
                self.tableView.reloadData()
            case .error(let message):
                if self.requests.count < 1 {
                    self.tableView.separatorStyle = .none
                    self.requestCountLabel.text = message
                }
            case .empty:
                self.tableView.separatorStyle = .none
                self.requestCountLabel.text = "Este repositório não possui pull requests"
            case .paginating:
                // state representation
                break
            }
        }
    }

    override func viewDidLoad() {
        self.navigationItem.title = repository?.fullName
        self.currentState = .loading
        self.setupRequestCountLabel()
    }

    fileprivate var openRequests: Int = 0
    fileprivate var closedRequests: Int = 0
    fileprivate func setupRequestCountLabel() {
        guard let repository = self.repository else {
            return
        }

        provider?.fetchRequestStatus(for: repository, status: .open, result: { (openResult) in
            switch openResult {
            case .success(let open):
                self.openRequests = open.count
                self.provider?.fetchRequestStatus(for: repository, status: .closed, result: { (closedResult) in
                    switch closedResult {
                    case .success(let closed):
                        self.closedRequests = closed.count
                        // swiftlint:disable line_length
                        let openedString = NSAttributedString(string: "\(String(self.openRequests)) opened",
                                                            attributes: [NSAttributedStringKey.foregroundColor: UIColor.orange])
                        let closedString = NSAttributedString(string: " / \(String(self.closedRequests)) closed")
                        let attributed = NSMutableAttributedString(attributedString: openedString)
                        attributed.append(closedString)
                        // swiftlint:enable line_length
                        self.requestCountLabel.attributedText = attributed
                        self.fetchRequests()
                    case .error:
                        break
                    }
                })
            case .error:
                break
            }
        })
    }

    fileprivate var currentPage: Int = 0
    func fetchRequests() {
        // if there are no PRs, there's no need to fetch them
        guardRequestsExist()

        guard let repo = repository else {
            return
        }

        if self.currentState == .paginating {
            return
        }
        self.currentState = .paginating

        currentPage += 1
        provider?.fetchRequests(for: repo, pageNumber: currentPage, result: { (result) in
            switch result {
            case .success(let requests):
                self.setupViewForRequests(requests)
            case .error:
                self.currentState = .error("Ocorreu um erro de conexão")
            }
        })
    }

    fileprivate func setupViewForRequests(_ requests: [PullRequest]) {
        self.requests += requests
        if self.dataSource != nil, self.delegate != nil {
            self.dataSource?.data += requests
        } else {
            self.dataSource = PullRequestListDataSource(self.tableView, requests: requests)
            self.delegate = PullRequestListTableViewDelegate(self)
            self.tableView.dataSource = self.dataSource
            self.tableView.delegate = self.delegate
            self.currentState = .ready
        }
    }

    fileprivate func guardRequestsExist() {
        guard openRequests > 0 || closedRequests > 0 else {
            currentState = .empty
            return
        }
    }
}
